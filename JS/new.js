

let arr = ["hello", "world", [1, 2, 3, 4, 5], "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.body;


function createList(arr, parent){
    const list = document.createElement("ol");
    for (let i = 0; i < arr.length; i++) {
        let li = document.createElement("li");
        if(Array.isArray(arr[i])) {
            const subList = document.createElement("ul");
            for (let j = 0; j < arr[i].length; j++) {
                let subLi = document.createElement("li");
                subLi.innerText = arr[i][j];
                subList.append(subLi)
            }
            
            li.append(subList);
        }else{
            li.innerText = arr[i];
        }

        list.append(li)
    }
    setTimeout(() => list.remove(), 3000)
    return parent.append(list)
}

createList(arr, parent);